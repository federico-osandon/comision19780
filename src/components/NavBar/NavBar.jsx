// import {Navbar, Container, Nav, NavDropdown} from 'react-bootstrap'
import { Link, NavLink } from 'react-router-dom'
import Navbar from 'react-bootstrap/Navbar'
import Container from 'react-bootstrap/Container'
import Nav from 'react-bootstrap/Nav'
import NavDropdown from 'react-bootstrap/NavDropdown'
import CartWidget from './CartWidget'



function NavBar() {
    return (
        <>
            <Navbar collapseOnSelect expand="lg" bg="dark" variant="dark">
                <Container>
                    <Link to='/'>React-Ecommerce</Link>
                    <Navbar.Toggle aria-controls="responsive-navbar-nav" />
                    <Navbar.Collapse id="responsive-navbar-nav">
                        <Nav className="me-auto">
                            <Link className='' to="/categoria/gorras">Gorras</Link>
                            <NavLink className="" activeClassName='' to="/categoria/remeras">Remeras</NavLink>                       
                            <CartWidget />
                        </Nav>                        
                    </Navbar.Collapse>
                    <Link to="/cart" >Carrito</Link>                       
                </Container>
            </Navbar>
        </>
    )
}

export default NavBar
